# Docker Ansible

Creates a docker image for deploying Ansible and provide linting support

## Package versions

Ansible: 4.7.0
Ansible-core: 2.11.8
Ansible-hashivault-module: 4.6.6
Ansible-lint: 5.3.2

Or
Ansible: 10.4.0
Ansible-core: 2.17.5
Ansible-hashivault-module: 5.3.0
Ansible-lint: 24.9.2

## Tags

We use the following tags
- .latest: the most recent build
- .stable: the tested and tagged stable version (recommended)
- .<version>: Use a specific version of past stable releases

# Build
Any pushes to main branch will create a new .latest
Any tags will create a .stable and a .<tag> version
