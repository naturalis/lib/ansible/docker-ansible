FROM willhallonline/ansible:2.17-alpine-3.19

RUN apk update && apk upgrade --no-cache && \
  apk add --no-cache curl jq && \
  pip install ansible-modules-hashivault==5.3.0 netaddr==1.3.0
